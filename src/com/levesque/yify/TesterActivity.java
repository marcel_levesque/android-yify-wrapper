package com.levesque.yify;

import java.io.IOException;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;

import com.levesque.wrapper.yify.YIFY;
import com.levesque.wrapper.yifyapi.collections.MovieCommentList;
import com.levesque.wrapper.yifyapi.collections.MovieList;
import com.levesque.wrapper.yifyapi.collections.RequestList;
import com.levesque.wrapper.yifyapi.collections.UpcomingMovieList;
import com.levesque.wrapper.yifyapi.model.Login;
import com.levesque.wrapper.yifyapi.model.MovieDetails;
import com.levesque.wrapper.yifyapi.model.ProfileDetails;
import com.levesque.wrapper.yifyapi.model.UserInfo;


/**
 * Tester Class for YIFY API
 * @author Marcel Levesque
 *
 */
public class TesterActivity extends Activity {

	final YIFY yify = new YIFY();
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		final LoadMovieDetails lmd = new LoadMovieDetails();
		lmd.execute();
		final LoadUpcomingMovies lum = new LoadUpcomingMovies();
		lum.execute();
		final LoadMovieList lml = new LoadMovieList();
		lml.execute();
		final LoadMovieComments lmc = new LoadMovieComments();
		lmc.execute();
		final LoadUserInfo lui = new LoadUserInfo();
		lui.execute();
		final LoadProfileDetails lpd = new LoadProfileDetails();
		lpd.execute();
		final UserLogin ul = new UserLogin();
		ul.execute();
		final LoadRequestList lrl = new LoadRequestList();
		lrl.execute();
		
	}

	// Testing API movie details Function.
	private class LoadMovieDetails extends AsyncTask<Void, Void, MovieDetails> {

		protected MovieDetails doInBackground(Void... params) {

			try {
				return yify.getMovieDetails(353);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}

		@Override
		protected void onPostExecute(MovieDetails result) {
			
			System.out.println("Testing Movie details...");
			System.out.println(result.getMovieId());
			System.out.println(result.getMovieTitle());
			System.out.println(result.getImdbId());
			System.out.println(result.getDateUploaded());

		}

	} // End LoadMovieDetails

	
	// Testing API UpcomingMovies Function
	private class LoadUpcomingMovies extends
			AsyncTask<Void, Void, UpcomingMovieList> {

		@Override
		protected UpcomingMovieList doInBackground(Void... params) {

			try {
				return yify.getUpcomingMovies();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}

		@Override
		protected void onPostExecute(UpcomingMovieList result) {
			
			System.out.println("Testing Upcoming Movies...");
			for (int i = 0; i < result.size(); i++) {
				System.out.println(result.get(i).getMovieTitle());
				System.out.println(result.get(i).getMovieCover());
				System.out.println(result.get(i).getImdbId());
				System.out.println(result.get(i).getImdbLink());
				System.out.println(result.get(i).getUploader());
				System.out.println(result.get(i).getDateAdded());
			}
		}

	} // End LoadUpcomingMovies
	
	// Testing API MovieList Function
		private class LoadMovieList extends AsyncTask<Void, Void, MovieList> {

			protected MovieList doInBackground(Void... params) {

				try {
					return yify.getMovieList(0, 0, "", 0, "Indiana jones", "", "","");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				}
			}

			@Override
			protected void onPostExecute(MovieList result) {
				
				System.out.println("Testing Movie Listing...");
				for (int i = 0; i < result.getMovieList().size(); i++) {
					System.out.println(result.getMovieList().get(i).getMovieTitle());
				}
				System.out.println("Movie count = " + result.getMovieCount());
			}

		} // End LoadMovieList
		
		
		// Testing API Comments Function
		private class LoadMovieComments extends
				AsyncTask<Void, Void, MovieCommentList> {

			@Override
			protected MovieCommentList doInBackground(Void... params) {

				try {
					return yify.getMovieCommentList(353);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				}
			}

			@Override
			protected void onPostExecute(MovieCommentList result) {
				
				
				System.out.println("Testing Movie comments...");
				for (int i = 0; i < result.size(); i++) {
					System.out.println(result.get(i).getCommentId());
					System.out.println(result.get(i).getCommentText());
					System.out.println(result.get(i).getUserName());
					System.out.println(result.get(i).getDateAdded());
					System.out.println(result.get(i).getUserGroup());
					System.out.println(result.get(i).getUserId());
				}
			}

		} // End LoadMovieComments
		
		
		// Testing API User Info Function
		private class LoadUserInfo extends AsyncTask<Void, Void, UserInfo> {

			protected UserInfo doInBackground(Void... params) {

				try {
					return yify.getUserInfo(16);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				}
			}

			@Override
			protected void onPostExecute(UserInfo result) {
				
				System.out.println("Testing User Info");
				System.out.println(result.getUserName());
				System.out.println(result.getUserRole());
				System.out.println(result.getAbout());
				System.out.println(result.getJoinDate());

			}

		} // End LoadUserInfo
		
		// Testing API Profile Details Function
		private class LoadProfileDetails extends AsyncTask<Void, Void, ProfileDetails> {

			protected ProfileDetails doInBackground(Void... params) {

				try {
					return yify.getProfileDetails("User_hash_number");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				}
			}

			@Override
			protected void onPostExecute(ProfileDetails result) {
				
				System.out.println("Testing Profile details...");
				System.out.println(result.getUserName());
				System.out.println(result.getUserRole());
				//System.out.println(result.getAbout());
				System.out.println(result.getJoinDate());

			}

		} // End LoadProfile Details
		
		
		
		// Testing user login class
		private class UserLogin extends AsyncTask<Void, Void, Login> {

			protected Login doInBackground(Void... params) {

				try {
					return yify.userLogin("username", "password");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				}
			}

			@Override
			protected void onPostExecute(Login result) {
				
				System.out.println("Testing User Login...");
				System.out.println(result.getUsername());
				System.out.println(result.getHash());
				System.out.println(result.getUserId());
			}

		} // End LoadUserInfo

    // Testing API RequestList Function
        private class LoadRequestList extends AsyncTask<Void, Void, RequestList> {

            protected RequestList doInBackground(Void... params) {

                try {
                    return yify.getRequestList("confirmed", 0, 0, "votes", "asc");
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(RequestList result) {

                System.out.println("Testing Request Listing...");
                for (int i = 0; i < result.getRequestList().size(); i++) {
                    System.out.println(result.getRequestList().get(i).getMovieTitle());
                }
                System.out.println("Movie count = " + result.getMovieCount());
            }

        } // End LoadRequestList
}
