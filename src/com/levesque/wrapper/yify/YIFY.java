/*
 *      Copyright (c) 2013 Marcel Levesque
 *
 *      This file is part of YIFY API.
 *
 *      YIFY API is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      any later version.
 *
 *      YIFY API is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with YIFY API.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.levesque.wrapper.yify;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.levesque.wrapper.yifyapi.collections.MovieCommentList;
import com.levesque.wrapper.yifyapi.collections.MovieList;
import com.levesque.wrapper.yifyapi.collections.UpcomingMovieList;
import com.levesque.wrapper.yifyapi.model.Login;
import com.levesque.wrapper.yifyapi.model.MovieDetails;
import com.levesque.wrapper.yifyapi.model.ProfileDetails;
import com.levesque.wrapper.yifyapi.model.UserInfo;
import com.levesque.wrapper.yifyapi.tools.HtttpRetriever;
import com.levesque.wrapper.yifyapi.tools.UrlBuilder;
import com.levesque.wrapper.yifyapi.collections.RequestList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.levesque.wrapper.yifyapi.tools.UrlBuilder.PARAM_COMMENTMOVIEID;
import static com.levesque.wrapper.yifyapi.tools.UrlBuilder.PARAM_GENRE;
import static com.levesque.wrapper.yifyapi.tools.UrlBuilder.PARAM_HASH;
import static com.levesque.wrapper.yifyapi.tools.UrlBuilder.PARAM_KEYWORDS;
import static com.levesque.wrapper.yifyapi.tools.UrlBuilder.PARAM_LIMIT;
import static com.levesque.wrapper.yifyapi.tools.UrlBuilder.PARAM_MOVIEID;
import static com.levesque.wrapper.yifyapi.tools.UrlBuilder.PARAM_ORDER;
import static com.levesque.wrapper.yifyapi.tools.UrlBuilder.PARAM_PAGE;
import static com.levesque.wrapper.yifyapi.tools.UrlBuilder.PARAM_QUALITY;
import static com.levesque.wrapper.yifyapi.tools.UrlBuilder.PARAM_RATING;
import static com.levesque.wrapper.yifyapi.tools.UrlBuilder.PARAM_SET;
import static com.levesque.wrapper.yifyapi.tools.UrlBuilder.PARAM_SORT;
import static com.levesque.wrapper.yifyapi.tools.UrlBuilder.PARAM_USERID;

/**
 * YIFY API <p> The API is described here: http://yify-torrents.com/api/
 * 
 * @author Marcel Levesque
 *
 */
public class YIFY {
	
	private final String BASE_API_URL = "http://yify-torrents.com/api/";
	//API methods
	private static final String BASE_MOVIELIST = "list.json";
	private static final String BASE_UPCOMINGMOVIES = "upcoming.json";
	private static final String BASE_MOVIEDETAILS = "movie.json";
	private static final String BASE_MOVIECOMMENTS = "comments.json";
	private static final String BASE_USERINFO = "user.json";
	private static final String BASE_LOGIN = "login.json";
	private static final String BASE_PROFILEDETAILS = "profile.json";
    private static final String BASE_REQUESTLIST= "requests.json";
	// Mapper used to map API responses
	private ObjectMapper mapper = new ObjectMapper();
	// Generic http retriever object
	private final HtttpRetriever httpRetriever = new HtttpRetriever();

	String stringMovieList = "{\"MovieCount\":\"94\",\"MovieList\":[{\"MovieID\":\"3582\",\"MovieUrl\":\"http://yify-torrents.com/movie/Hansel_and_Gretel_Witch_Hunters_2013_3D\",\"MovieTitle\":\"Hansel and Gretel: Witch Hunters (2013) 3D\",\"DateUploaded\":\"2013-05-21 02:37:05\",\"Quality\":\"3D\",\"CoverImage\":\"http://static.yify-torrents.com/attachments/Hansel_and_Gretel_Witch_Hunters_2013_3D/hansel_med.png\",\"ImdbCode\":\"tt1428538\",\"ImdbLink\":\"http://www.imdb.com/title/tt1428538/\",\"Size\":\"1.24 GB\",\"MovieRating\":\"6.1\",\"Genre\":\"Action\",\"Downloaded\":\"12690\",\"TorrentSeeds\":\"469\",\"TorrentPeers\":\"462\",\"TorrentUrl\":\"http://yify-torrents.com/download/start/hanselgret3d.torrent\",\"TorrentHash\":\"6dc8174b96f7b9b4a56acb7b248ca80454ebab6b\",\"TorrentMagnetUrl\":\"magnet:?xt=urn:btih:6dc8174b96f7b9b4a56acb7b248ca80454ebab6b&dn=Hansel+and+Gretel:+Witch+Hunters+(2013)+3D&tr=http://exodus.desync.com:6969/announce&tr=udp://tracker.openbittorrent.com:80/announce&tr=udp://tracker.1337x.org:80/announce&tr=udp://exodus.desync.com:6969/announce&tr=udp://tracker.yify-torrents.com/announce\"}]}";
	
	/**
	 * This method is used to retrieve all the upcoming movies.
	 * 
	 * @return Upcoming Movies
	 * @throws IOException
	 */
	public UpcomingMovieList getUpcomingMovies() throws IOException {
		UrlBuilder url = new UrlBuilder(BASE_UPCOMINGMOVIES);
		String response = httpRetriever.getJSON(url.buildUrlString());
		return mapper.readValue(response, UpcomingMovieList.class);
	}

	/**
	 * This method is used to retrieve the movie details. 
	 * 
	 * @param movieId  
	 * @return Movie details
	 * @throws IOException
	 */
	public MovieDetails getMovieDetails(int movieId) throws IOException {
		UrlBuilder url = new UrlBuilder(BASE_MOVIEDETAILS);
		url.addArgument(PARAM_MOVIEID, movieId);
		String response = httpRetriever.getJSON(url.buildUrlString());
		return mapper.readValue(response, MovieDetails.class);
	}

	/**
	 * This method is used to retrieve a list of movies that matches the parameters
	 * given. This method can be useful to find a specific movie by entering 
	 * the movie title as keyword parameter.
	 * 
	 * @param limit Max amount of movie results
	 * @param set  Next set of movies 
	 * @param quality  Select a quality type to filter by (720p, 1080p, 3D)
	 * @param rating  Minimum movie rating for display (0-9)
	 * @param keyword  Matching keywords search
	 * @param genre  Movies from chosen type genre
	 * @param sort  Sorts the results by choose method(date, seeds, peers, size, alphabet, rating, downloaded)
	 * @param order  Orders the results with either ascending or descending (desc, asc)
	 * @return Movielist containing the movies that matches the parameter given.
	 * @throws IOException
	 */
	public MovieList getMovieList(int limit, int set, String quality,
			int rating, String keyword, String genre, String sort, String order)
			throws IOException {

		UrlBuilder url = new UrlBuilder(BASE_MOVIELIST);
		if (limit > 0) {
			url.addArgument(PARAM_LIMIT, limit);
		}
		if (set > 1) {
			url.addArgument(PARAM_SET, set);
		}
		if (!quality.matches("")) {
			url.addArgument(PARAM_QUALITY, quality);
		}
		if (rating > 0) {
			url.addArgument(PARAM_RATING, rating);
		}
		if (!keyword.matches("")) {
			url.addArgument(PARAM_KEYWORDS, keyword);
		}
		if (!genre.matches("")) {
			url.addArgument(PARAM_GENRE, genre);
		}
		if (!sort.matches("")) {
			url.addArgument(PARAM_SORT, sort);
		}
		if (!order.matches("")) {
			url.addArgument(PARAM_ORDER, order);
		}

		String response = httpRetriever.getJSON(url.buildUrlString());
		return mapper.readValue(response, MovieList.class);
		// return mapper.readValue(stringMovieList, MovieList.class);
	}

	/**
	 * This method retrieve all the comments made on that specific film.
	 * 
	 * @param movieId  
	 * @return
	 * @throws IOException
	 */
	public MovieCommentList getMovieCommentList(int movieId) throws IOException {
		UrlBuilder url = new UrlBuilder(BASE_MOVIECOMMENTS);
		url.addArgument(PARAM_COMMENTMOVIEID, movieId);
		String response = httpRetriever.getJSON(url.buildUrlString());
		return mapper.readValue(response, MovieCommentList.class);
	}

	/**
	 * This method retrieve the user information.
	 * 
	 * @param userId
	 * @return
	 * @throws IOException
	 */
	public UserInfo getUserInfo(int userId) throws IOException {
		UrlBuilder url = new UrlBuilder(BASE_USERINFO);
		url.addArgument(PARAM_USERID, userId);
		String response = httpRetriever.getJSON(url.buildUrlString());
		return mapper.readValue(response, UserInfo.class);
	}

	/**
	 * This method retrieve the user profile details.  User hash is required.
	 * 
	 * @param userHash
	 * @return
	 * @throws IOException
	 */
	public ProfileDetails getProfileDetails(String userHash) throws IOException {
		UrlBuilder url = new UrlBuilder(BASE_PROFILEDETAILS);
		url.addArgument(PARAM_HASH, userHash);
		String response = httpRetriever.getJSON(url.buildUrlString());
		return mapper.readValue(response, ProfileDetails.class);
	}

	/**
	 * This method can be used to login a user.
	 * 
	 * @param username  
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public Login userLogin(String username, String password) throws Exception {
		List<NameValuePair> postParameters = new ArrayList<NameValuePair>();
		postParameters.add(new BasicNameValuePair("username", username));
		postParameters.add(new BasicNameValuePair("password", password));
		String response = httpRetriever.executeHttpPost(postParameters, BASE_API_URL + BASE_LOGIN);
		return mapper.readValue(response, Login.class);
	}

    /**
     * This method is used to retrieve the list of rquested movies
     *
     * @param page determine whether the accepted or confirmed list is returned.
     * possible values are (accepted, confirmed).  This parameter is mandatory.
     * @param limit Max amount of movie results
     * @param set  Next set of movies
     * @param sort  Sorts the results by choose method(date, rating, or votes)
     * @param order  Orders the results with either ascending or descending (desc, asc)
     * @return the movie request list either accepted or confirmed based on page parameter value
     * @throws IOException
     */
    public RequestList getRequestList(String page, int limit, int set, String sort, String order)
            throws IOException {

        UrlBuilder url = new UrlBuilder(BASE_REQUESTLIST);
        if (!page.matches("")) {
            url.addArgument(PARAM_PAGE, page);
        }
        if (limit > 0) {
            url.addArgument(PARAM_LIMIT, limit);
        }
        if (set > 1) {
            url.addArgument(PARAM_SET, set);
        }
        if (!sort.matches("")) {
            url.addArgument(PARAM_SORT, sort);
        }
        if (!order.matches("")) {
            url.addArgument(PARAM_ORDER, order);
        }

        String response = httpRetriever.getJSON(url.buildUrlString());
        return mapper.readValue(response, RequestList.class);
    }

}
