/*
 *      Copyright (c) 2013 Marcel Levesque
 *
 *      This file is part of YIFY API.
 *
 *      YIFY API is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      any later version.
 *
 *      YIFY API is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with YIFY API.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.levesque.wrapper.yifyapi.tools;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;


/**
 * The API URL that is used to construct the API url string
 *
 * @author Marcel Levesque
 */
public class UrlBuilder {

    /*
     * TheMovieDbApi API Base URL
     */
    private static final String YIFY_API_BASE = "http://yify-torrents.com/api/";
    /*
     * Parameter configuration
     */
    private static final String DELIMITER_FIRST = "?";
    private static final String DELIMITER_SUBSEQUENT = "&";
    private static final String DEFAULT_STRING = "";
    /*
     * Properties
     */
    private String method;
    private Map<String, String> arguments = new HashMap<String, String>();
    /*
     * API Parameters
     */
    public static final String PARAM_LIMIT = "limit=";
    public static final String PARAM_SET = "set=";
    public static final String PARAM_QUALITY = "quality=";
    public static final String PARAM_RATING = "rating=";
    public static final String PARAM_KEYWORDS = "keywords=";
    public static final String PARAM_GENRE = "genre=";
    public static final String PARAM_SORT = "sort=";
    public static final String PARAM_ORDER = "order=";
    public static final String PARAM_MOVIEID = "id=";
    public static final String PARAM_COMMENTMOVIEID = "movieid=";
    public static final String PARAM_USERID = "id=";
    public static final String PARAM_USERNAME = "username=";
    public static final String PARAM_PASSWORD = "password=";
    public static final String PARAM_HASH = "hash=";
    public static final String PARAM_PAGE = "page=";
    
    
    /**
     * Constructor for the simple API URL method
     *
     * @param method
     */
    public UrlBuilder(String method) {
        this.method = method;
    }



	/**
	 * Build the URL from the pre-created arguments.
	 */
	public String buildUrlString() {
    	StringBuilder urlString = new StringBuilder(YIFY_API_BASE);

        // Get the start of the URL
        urlString.append(method);

        // Append the first delimiter ?
        urlString.append(DELIMITER_FIRST);

        for (Map.Entry<String, String> argEntry : arguments.entrySet()) {
		            urlString.append(DELIMITER_SUBSEQUENT).append(argEntry.getKey());
		            urlString.append(argEntry.getValue());
        }
        
		return urlString.toString().replaceAll(" ", "%20");
	}



    /**
     * Add arguments individually
     *
     * @param key
     * @param value
     */
    public void addArgument(String key, String value) {
        arguments.put(key, value);
    }

    /**
     * Add arguments individually
     *
     * @param key
     * @param value
     */
    public void addArgument(String key, int value) {
        arguments.put(key, Integer.toString(value));
    }

    /**
     * Add arguments individually
     *
     * @param key
     * @param value
     */
    public void addArgument(String key, boolean value) {
        arguments.put(key, Boolean.toString(value));
    }

    /**
     * Add arguments individually
     *
     * @param key
     * @param value
     */
    public void addArgument(String key, float value) {
        arguments.put(key, Float.toString(value));
    }

    /**
     * Clear the arguments
     */
    public void clearArguments() {
        arguments.clear();
    }

}

