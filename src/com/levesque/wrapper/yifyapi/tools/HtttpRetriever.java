/*
 *      Copyright (c) 2013 Marcel Levesque
 *
 *      This file is part of YIFY API.
 *
 *      YIFY API is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      any later version.
 *
 *      YIFY API is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with YIFY API.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.levesque.wrapper.yifyapi.tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.net.http.AndroidHttpClient;

public class HtttpRetriever {

	
	public String getJSON(String url) throws IOException {

		HttpGet post = new HttpGet(url);
		post.addHeader("accept", "application/json");
		post.addHeader("Content-length", "0");
		AndroidHttpClient client = AndroidHttpClient.newInstance("DATA");

		HttpResponse response = client.execute(post);

		HttpEntity httpEntity = response.getEntity();
		String result = EntityUtils.toString(httpEntity);
		client.close();
		return result;

	}

	public String executeHttpPost(List<NameValuePair> postParameters,
			String url) throws Exception {
		BufferedReader in = null;
		try {
			HttpClient client = new DefaultHttpClient();
			HttpPost request = new HttpPost(url);
			UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(
					postParameters);
			request.setEntity(formEntity);
			HttpResponse response = client.execute(request);
			in = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent()));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			in.close();
			String result = sb.toString();
			return result;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
