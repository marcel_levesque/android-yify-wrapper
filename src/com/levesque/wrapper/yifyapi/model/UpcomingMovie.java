/*
 *      Copyright (c) 2013 Marcel Levesque
 *
 *      This file is part of YIFY API.
 *
 *      YIFY API is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      any later version.
 *
 *      YIFY API is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with YIFY API.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.levesque.wrapper.yifyapi.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpcomingMovie implements Serializable{
	
	private static final long serialVersionUID = 7666575914162048957L;
	
	/*
	 * Properties
	 */
	@JsonProperty("MovieTitle")
	private String movieTitle;
	@JsonProperty("MovieCover")
	private String movieCover;
	@JsonProperty("ImdbCode")
	private String imdbId;
	@JsonProperty("ImdbLink")
	private String imdbLink;
	@JsonProperty("Uploader")
	private String uploader;
	@JsonProperty("DateAdded")
	private String dateAdded;
	
	public String getMovieTitle() {
		return movieTitle;
	}
	
	public void setMovieTitle(String movieTitle) {
		this.movieTitle = movieTitle;
	}
	
	public String getMovieCover() {
		return movieCover;
	}
	
	public void setMovieCover(String movieCover) {
		this.movieCover = movieCover;
	}
	
	public String getImdbId() {
		return imdbId;
	}
	
	public void setImdbId(String imdbId) {
		this.imdbId = imdbId;
	}
	
	public String getImdbLink() {
		return imdbLink;
	}
	
	public void setImdbLink(String imdbLink) {
		this.imdbLink = imdbLink;
	}
	
	public String getUploader() {
		return uploader;
	}
	
	public void setUploader(String uploader) {
		this.uploader = uploader;
	}
	
	public String getDateAdded() {
		return dateAdded;
	}
	
	public void setDateAdded(String dateAdded) {
		this.dateAdded = dateAdded;
	}
}
