/*
 *      Copyright (c) 2013 Marcel Levesque
 *
 *      This file is part of YIFY API.
 *
 *      YIFY API is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      any later version.
 *
 *      YIFY API is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with YIFY API.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.levesque.wrapper.yifyapi.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MovieComment implements Serializable{

	private static final long serialVersionUID = 7785995476384343163L;
	
	/*
	 * Properties
	 */
	@JsonProperty("CommentID")
	private String commentId;
	@JsonProperty("CommentText")
	private String commentText;
	@JsonProperty("DateAdded")
	private String dateAdded;
	@JsonProperty("UserID")
	private String userId;
	@JsonProperty("ParentCommentID")
	private String parentCommentId;
	@JsonProperty("UserName")
	private String userName;
	@JsonProperty("UserGroup")
	private String userGroup;


	public String getCommentId() {
		return commentId;
	}

	public void setCommentId(String commentId) {
		this.commentId = commentId;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public String getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(String dateAdded) {
		this.dateAdded = dateAdded;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getParentCommentId() {
		return parentCommentId;
	}

	public void setParentCommentId(String parentCommentId) {
		this.parentCommentId = parentCommentId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserGroup() {
		return userGroup;
	}

	public void setUserGroup(String userGroup) {
		this.userGroup = userGroup;
	}

}


