/*
 *      Copyright (c) 2013 Marcel Levesque
 *
 *      This file is part of YIFY API.
 *
 *      YIFY API is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      any later version.
 *
 *      YIFY API is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with YIFY API.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.levesque.wrapper.yifyapi.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MovieDetails implements Serializable{

	private static final long serialVersionUID = 7593169077311982533L;
	/*
	 * Properties
	 */
	@JsonProperty("MovieID")
	private String movieId;
	@JsonProperty("MovieUrl")
	private String movieUrl;
	@JsonProperty("DateUploaded")
	private String dateUploaded;
	@JsonProperty("Uploader")
	private String uploader;
	@JsonProperty("UploaderNotes")
	private String uploaderNotes;
	@JsonProperty("Quality")
	private String quality;
	@JsonProperty("Resolution")
	private String resolution;
	@JsonProperty("FrameRate")
	private String frameRate;
	@JsonProperty("Language")
	private String language;
	@JsonProperty("Subtitles")
	private String subtitles;
	@JsonProperty("MediumCover")
	private String mediumCover;
	@JsonProperty("LargeCover")
	private String largeCover;
	@JsonProperty("LargeScreenshot1")
	private String largeScreenshot1;
	@JsonProperty("LargeScreenshot2")
	private String largeScreenshot2;
	@JsonProperty("LargeScreenshot3")
	private String largeScreenshot3;
	@JsonProperty("MediumScreenshot1")
	private String mediumScreenshot1;
	@JsonProperty("MediumScreenshot2")
	private String mediumScreenshot2;
	@JsonProperty("MediumScreenshot3")
	private String mediumScreenshot3;
	@JsonProperty("ImdbCode")
	private String imdbId;
	@JsonProperty("ImdbLink")
	private String imdbLink;
	@JsonProperty("MovieTitle")
	private String movieTitle;
	@JsonProperty("MovieRating")
	private String movieRating;
	@JsonProperty("MovieRuntime")
	private String movieRuntime;
	@JsonProperty("YoutubeTrailerID")
	private String youTubeTrailerId;
	@JsonProperty("YoutubeTrailerUrl")
	private String youTubeTrailerUrl;
	@JsonProperty("AgeRating")
	private String mpaaRating;
	@JsonProperty("Genre1")
	private String genre1;
	@JsonProperty("Genre2")
	private String genre2;
	@JsonProperty("ShortDescription")
	private String shortDescription;
	@JsonProperty("LongDescription")
	private String longDescription;
	@JsonProperty("Downloaded")
	private String downloaded;
	@JsonProperty("TorrentUrl")
	private String torrentUrl;
	@JsonProperty("TorrentHash")
	private String torrentHash;
	@JsonProperty("TorrentMagnetUrl")
	private String torrentMagnetUrl;
	@JsonProperty("TorrentSeeds")
	private String torrentSeeds;
	@JsonProperty("TorrentPeers")
	private String torrentPeers;
	@JsonProperty("Size")
	private String size;
	
	
	public String getMovieId() {
		return movieId;
	}
	
	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}
	
	public String getMovieUrl() {
		return movieUrl;
	}
	
	public void setMovieUrl(String MovieUrl) {
		this.movieUrl = MovieUrl;
	}
	
	public String getDateUploaded() {
		return dateUploaded;
	}

	public void setDateUploaded(String dateUploaded) {
		this.dateUploaded = dateUploaded;
	}
	public String getUploader() {
		return uploader;
	}
	
	public void setUploader(String uploader) {
		this.uploader = uploader;
	}
	public String getUploaderNotes() {
		return uploaderNotes;
	}

	public void setUploaderNotes(String uploaderNotes) {
		this.uploaderNotes = uploaderNotes;
	}
	public String getQuality() {
		return quality;
	}
	
	public void setQuality(String quality) {
		this.quality = quality;
	}
	public String getResolution() {
		return resolution;
	}
	
	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	public String getFrameRate() {
		return frameRate;
	}
	
	public void setFrameRate(String frameRate) {
		this.frameRate = frameRate;
	}
	public String getLanguage() {
		return language;
	}
	
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getSubtitles() {
		return subtitles;
	}
	
	public void setSubtitles(String subtitles) {
		this.subtitles = subtitles;
	}
	public String getMediumCover() {
		return mediumCover;
	}
	
	public void setMediumCover(String mediumCover) {
		this.mediumCover = mediumCover;
	}
	public String getLargeCover() {
		return largeCover;
	}
	
	public void setLargeCover(String largeCover) {
		this.largeCover = largeCover;
	}
	public String getLargeScreenshot1() {
		return largeScreenshot1;
	}
	
	public void setLargeScreenshot1(String largeScreenshot1) {
		this.largeScreenshot1 = largeScreenshot1;
	}
	public String getLargeScreenshot2() {
		return largeScreenshot2;
	}
	
	public void setLargeScreenshot2(String largeScreenshot2) {
		this.largeScreenshot2 = largeScreenshot2;
	}
	public String getLargeScreenshot3() {
		return largeScreenshot3;
	}
	
	public void setLargeScreenshot3(String largeScreenshot3) {
		this.largeScreenshot3 = largeScreenshot3;
	}
	public String getMediumScreenshot1() {
		return mediumScreenshot1;
	}
	
	public void setMediumScreenshot1(String mediumScreenshot1) {
		this.mediumScreenshot1 = mediumScreenshot1;
	}
	public String getMediumScreenshot2() {
		return mediumScreenshot2;
	}
	
	public void setMediumScreenshot2(String mediumScreenshot2) {
		this.mediumScreenshot2 = mediumScreenshot2;
	}
	public String getMediumScreenshot3() {
		return mediumScreenshot3;
	}
	
	public void setMediumScreenshot3(String mediumScreenshot3) {
		this.mediumScreenshot3 = mediumScreenshot3;
	}
	public String getImdbId() {
		return imdbId;
	}
	
	public void setImdbId(String imdbId) {
		this.imdbId = imdbId;
	}
	public String getImdbLink() {
		return imdbLink;
	}
	
	public void setImdbLink(String imdbLink) {
		this.imdbLink = imdbLink;
	}
	public String getMovieTitle() {
		return movieTitle;
	}
	
	public void setMovieTitle(String movieTitle) {
		this.movieTitle = movieTitle;
	}
	public String getMovieRating() {
		return movieRating;
	}
	
	public void setMovieRating(String movieRating) {
		this.movieRating = movieRating;
	}
	public String getMovieRuntime() {
		return movieRuntime;
	}
	
	public void setMovieRuntime(String movieRuntime) {
		this.movieRuntime = movieRuntime;
	}
	public String getYouTubeTrailerId() {
		return youTubeTrailerId;
	}
	
	public void setYouTubeTrailerId(String youTubeTrailerId) {
		this.youTubeTrailerId = youTubeTrailerId;
	}
	public String getYouTubeTrailerUrl() {
		return youTubeTrailerUrl;
	}
	
	public void setYouTubeTrailerUrl(String youTubeTrailerUrl) {
		this.youTubeTrailerUrl = youTubeTrailerUrl;
	}
	public String getMpaaRating() {
		return mpaaRating;
	}
	
	public void setMpaaRating(String mpaaRating) {
		this.mpaaRating = mpaaRating;
	}
	public String getGenre1() {
		return genre1;
	}
	
	public void setGenre1(String genre1) {
		this.genre1 = genre1;
	}
	public String getGenre2() {
		return genre2;
	}
	
	public void setGenre2(String genre2) {
		this.genre2 = genre2;
	}
	public String getShortDescription() {
		return shortDescription;
	}
	
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	public String getLongDescription() {
		return longDescription;
	}
	
	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}
	public String getDownloaded() {
		return downloaded;
	}
	
	public void setDownloaded(String downloaded) {
		this.downloaded = downloaded;
	}
	public String getTorrentUrl() {
		return torrentUrl;
	}
	
	public void setTorrentUrl(String torrentUrl) {
		this.torrentUrl = torrentUrl;
	}
	public String getTorrentHash() {
		return torrentHash;
	}
	
	public void setTorrentHash(String torrentHash) {
		this.torrentHash = torrentHash;
	}
	public String getTorrentMagnetUrl() {
		return torrentMagnetUrl;
	}
	
	public void setTorrentMagnetUrl(String torrentMagnetUrl) {
		this.torrentMagnetUrl = torrentMagnetUrl;
	}
	public String getTorrentSeeds() {
		return torrentSeeds;
	}
	
	public void setTorrentSeeds(String torrentSeeds) {
		this.torrentSeeds = torrentSeeds;
	}
	public String getTorrentPeers() {
		return torrentPeers;
	}
	
	public void setTorrentPeers(String torrentPeers) {
		this.torrentPeers = torrentPeers;
	}
	public String getSize() {
		return size;
	}
	
	public void setSize(String size) {
		this.size = size;
	}
	
}
