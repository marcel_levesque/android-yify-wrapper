/*
 *      Copyright (c) 2013 Marcel Levesque
 *
 *      This file is part of YIFY API.
 *
 *      YIFY API is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      any later version.
 *
 *      YIFY API is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with YIFY API.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.levesque.wrapper.yifyapi.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProfileDetails implements Serializable{

	private static final long serialVersionUID = -8092232284710712636L;

	/*
	 * Properties
	 */
	@JsonProperty("UserID")
	private String userId;
	@JsonProperty("UserName")
	private String userName;
	@JsonProperty("JoinDated")
	private String joinDate;
	@JsonProperty("LastSeenDate")
	private String lastSeenDate;
	@JsonProperty("IpAddress")
	private String ipAddress;
	@JsonProperty("NumOfVotesLeft")
	private String numOfVotesLeft;
	@JsonProperty("NumOfRequestsLeft")
	private String numOfRequestLeft;
	@JsonProperty("ProfileActive")
	private String profileActive;
	@JsonProperty("TorrentsDownloadedCount")
	private int torrentDownloadedCount;
	@JsonProperty("MoviesRequestedCount")
	private int movieRequestedCount;
	@JsonProperty("CommentCount")
	private int commentCount;
	@JsonProperty("ChatTimeSeconds")
	private String chatTimeSeconds;
	@JsonProperty("Avatar")
	private String avatarLink;
	@JsonProperty("About")
	private String about;
	@JsonProperty("UserRole")
	private String userRole;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(String joinDate) {
		this.joinDate = joinDate;
	}
	public String getLastSeenDate() {
		return lastSeenDate;
	}
	public void setLastSeenDate(String lastSeenDate) {
		this.lastSeenDate = lastSeenDate;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getNumOfVotesLeft() {
		return numOfVotesLeft;
	}
	public void setNumOfVotesLeft(String numOfVotesLeft) {
		this.numOfVotesLeft = numOfVotesLeft;
	}
	public String getNumOfRequestLeft() {
		return numOfRequestLeft;
	}
	public void setNumOfRequestLeft(String numOfRequestLeft) {
		this.numOfRequestLeft = numOfRequestLeft;
	}
	public String getProfileActive() {
		return profileActive;
	}
	public void setProfileActive(String profileActive) {
		this.profileActive = profileActive;
	}
	public int getTorrentDownloadedCount() {
		return torrentDownloadedCount;
	}
	public void setTorrentDownloadedCount(int torrentDownloadedCount) {
		this.torrentDownloadedCount = torrentDownloadedCount;
	}
	public int getMovieRequestedCount() {
		return movieRequestedCount;
	}
	public void setMovieRequestedCount(int movieRequestedCount) {
		this.movieRequestedCount = movieRequestedCount;
	}
	public int getCommentCount() {
		return commentCount;
	}
	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}
	public String getChatTimeSeconds() {
		return chatTimeSeconds;
	}
	public void setChatTimeSeconds(String chatTimeSeconds) {
		this.chatTimeSeconds = chatTimeSeconds;
	}
	public String getAvatarLink() {
		return avatarLink;
	}
	public void setAvatarLink(String avatarLink) {
		this.avatarLink = avatarLink;
	}
	public String getAbout() {
		return about;
	}
	public void setAbout(String about) {
		this.about = about;
	}
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	
}
