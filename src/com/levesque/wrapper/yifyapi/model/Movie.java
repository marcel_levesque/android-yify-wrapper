/*
 *      Copyright (c) 2013 Marcel Levesque
 *
 *      This file is part of YIFY API.
 *
 *      YIFY API is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      any later version.
 *
 *      YIFY API is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with YIFY API.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.levesque.wrapper.yifyapi.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Movie implements Serializable{
	
	private static final long serialVersionUID = -5721469710387800223L;
	
	/*
	 * Properties
	 */
	@JsonProperty("MovieID")
	private String movieId;
	@JsonProperty("MovieUrl")
	private String movieUrl;
	@JsonProperty("MovieTitle")
	private String movieTitle;
	@JsonProperty("DateUploaded")
	private String dateUploaded;
	@JsonProperty("Quality")
	private String quality;
	@JsonProperty("CoverImage")
	private String coverImage;
	@JsonProperty("ImdbCode")
	private String imdbId;
	@JsonProperty("ImdbLink")
	private String imdbLink;
	@JsonProperty("Size")
	private String size;
	@JsonProperty("MovieRating")
	private String movieRating;
	@JsonProperty("Genre")
	private String genre;
	@JsonProperty("TorrentSeeds")
	private String torrentSeeds;
	@JsonProperty("Downloaded")
	private String downloaded;
	@JsonProperty("TorrentPeers")
	private String torrentPeers;
	@JsonProperty("TorrentUrl")
	private String torrentUrl;
	@JsonProperty("TorrentHash")
	private String torrentHash;
	@JsonProperty("TorrentMagnetUrl")
	private String torrentMagnetUrl;
	
	
	public String getMovieId() {
		return movieId;
	}
	
	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}
	
	public String getMovieUrl() {
		return movieUrl;
	}
	
	public void setMovieUrl(String movieUrl) {
		this.movieUrl = movieUrl;
	}
	
	public String getMovieTitle() {
		return movieTitle;
	}
	
	public void setMovieTitle(String movieTitle) {
		this.movieTitle = movieTitle;
	}
	
	public String getDataUploaded() {
		return dateUploaded;
	}
	
	public void setDataUploaded(String dataUploaded) {
		this.dateUploaded = dataUploaded;
	}
	
	public String getQuality() {
		return quality;
	}
	
	public void setQuality(String quality) {
		this.quality = quality;
	}
	
	public String getCoverImage() {
		return coverImage;
	}
	
	public void setCoverImage(String coverImage) {
		this.coverImage = coverImage;
	}
	
	public String getImdbId() {
		return imdbId;
	}
	
	public void setImdbId(String imdbId) {
		this.imdbId = imdbId;
	}
	
	public String getImdbLink() {
		return imdbLink;
	}
	
	public void setImdbLink(String imdbLink) {
		this.imdbLink = imdbLink;
	}
	
	public String getSize() {
		return size;
	}
	
	public void setSize(String size) {
		this.size = size;
	}
	
	public String getMovieRating() {
		return movieRating;
	}
	
	public void setMovieRating(String movieRating) {
		this.movieRating = movieRating;
	}
	
	public String getGenre() {
		return genre;
	}
	
	public void setGenre(String genre) {
		this.genre = genre;
	}
	
	public String getDownloaded() {
		return downloaded;
	}
	
	public void setDownloaded(String downloaded) {
		this.downloaded = downloaded;
	}
	
	public String getTorrentSeeds() {
		return torrentSeeds;
	}
	
	public void setTorrentSeeds(String torrentSeeds) {
		this.torrentSeeds = torrentSeeds;
	}
	
	public String getTorrentPeers() {
		return torrentPeers;
	}
	
	public void setTorrentPeers(String torrentPeers) {
		this.torrentPeers = torrentPeers;
	}
	
	public String getTorrentUrl() {
		return torrentUrl;
	}
	
	public void setTorrentUrl(String torrentUrl) {
		this.torrentUrl = torrentUrl;
	}
	
	public String getTorrentHash() {
		return torrentHash;
	}
	
	public void setTorrentHash(String torrentHash) {
		this.torrentHash = torrentHash;
	}
	
	public String getTorrentMagnetUrl() {
		return torrentMagnetUrl;
	}
	
	public void setTorrentMagnetUrl(String torrentMagnetUrl) {
		this.torrentMagnetUrl = torrentMagnetUrl;
	}
	
}
