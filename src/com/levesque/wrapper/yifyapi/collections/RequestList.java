/*
 *      Copyright (c) 2013 Marcel Levesque
 *
 *      This file is part of YIFY API.
 *
 *      YIFY API is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation, either version 3 of the License, or
 *      any later version.
 *
 *      YIFY API is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with YIFY API.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.levesque.wrapper.yifyapi.collections;

import java.io.Serializable;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.levesque.wrapper.yifyapi.model.Request;

public class RequestList implements Serializable {

    private static final long serialVersionUID = -5765469710387800223L;
	/*
	 * Properties
	 */
	@JsonProperty("MovieCount")
	private String movieCount;
	@JsonProperty("RequestList")
	private ArrayList<Request> requestList;

	public String getMovieCount() {
		return movieCount;
	}
	public void setMovieCount(String movieCount) {
		this.movieCount = movieCount;
	}
	public ArrayList<Request> getRequestList() {
		return requestList;
	}
	public void setRequestList(ArrayList<Request> requestList) {
		this.requestList = requestList;
	}

}
