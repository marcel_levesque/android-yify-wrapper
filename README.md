The Android YIFY API

Author: Marcel Levesque (levesque.marcel AT Gmail DOT com)

This API uses the http://yify-torrents.com/api

Anyone can use it for their projects.

yify-torrents.com

This is an excellent torrent sites for movies.

See the TesterActivity.java files for examples on how to call the various implemented API function.

As of 1 July 2013 the following wrappers have been implemented:

- Upcoming Movies
- List Movies
- Movies Details
- Movie Comments
- Users Details
- Login
- Profile Details
- Request List